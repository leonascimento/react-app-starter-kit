import webpack from 'webpack';
import { resolve } from 'path';
import browserSyncPlugin from 'browser-sync-webpack-plugin';

export default {
  entry: [
    'webpack-hot-middleware/client',
    './src/vendors',
    './src/index'
  ],
  output: {
    path: resolve(__dirname, 'build'),
    publicPath: '/assets/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.sass$/,
        loader: 'style-loader!css-loader!sass?indentedSyntax=sass?sourceMap' 
      },
      {
        test: /\.scss$/,
        loader: 'css-loader!sass?sourceMap' 
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: "file-loader?name=images/img-[hash:6].[ext]"
      }
    ]
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['', '.js', '.jsx', '.sass', '.scss'],
    alias: {
      root: resolve(__dirname, 'src'),
      components: resolve(__dirname, 'src/components'),
      styles: resolve(__dirname, 'src/styles')
    }
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new browserSyncPlugin({
      host: 'localhost',
      port: 3000,
      proxy: 'http://localhost:4000'
    })
  ]
};

